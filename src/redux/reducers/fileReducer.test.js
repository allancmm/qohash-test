import fileReducer from "./fileReducer";
import * as actions from "../actions/fileActions";

it("should return list of files when passed SORT_FILES_SUCCESS", () => {
  // arrange
  const initialState = [];

  const listFiles = [{ nameDirectory: "mainDirectory" }];

  const action = actions.sortFilesSuccess(listFiles);

  // act
  const newState = fileReducer(initialState, action);

  // assert
  expect(newState.length).toEqual(1);
  expect(newState[0].nameDirectory).toEqual("mainDirectory");
});
