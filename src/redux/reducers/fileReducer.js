import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function fileReducer(state = initialState.files, action) {
  switch (action.type) {
    case types.SORT_FILES_SUCCESS:
      return action.listFiles;
    default:
      return state;
  }
}
