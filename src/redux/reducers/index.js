import { combineReducers } from "redux";
import files from "./fileReducer";
import apiCallsInProgress from "./apiStatusReducer";

const rootReducer = combineReducers({
  files,
  apiCallsInProgress,
});

export default rootReducer;
