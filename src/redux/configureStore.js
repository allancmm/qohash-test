// l'utilisation de CommonJS requis ci-dessous afin qu'on puisse importer dynamiquement pendant le build
if (process.env.NODE_ENV === "production") {
  module.exports = require("./configureStore.prod");
} else {
  module.exports = require("./configureStore.dev");
}
