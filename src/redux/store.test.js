import { createStore } from "redux";
import rootReducer from "./reducers";
import initialState from "./reducers/initialState";
import * as fileActions from "./actions/fileActions";

it("Should handle sending files", function () {
  // arrange
  const store = createStore(rootReducer, initialState);

  const listFiles = [];

  // act
  const action = fileActions.sortFilesSuccess(listFiles);
  store.dispatch(action);

  // assert
  const listFilesSorted = store.getState().listFiles;
  expect(listFilesSorted).not.toEqual(null);
});
