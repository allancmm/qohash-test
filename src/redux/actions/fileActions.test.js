import * as fileActions from "./fileActions";
import * as types from "./actionTypes";
import { listFiles } from "../../../tools/mockData";
import thunk from "redux-thunk";
import fetchMock from "fetch-mock";
import configureMockStore from "redux-mock-store";

// Test an async action
const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe("Async Actions", () => {
  afterEach(() => {
    fetchMock.restore();
  });

  describe("Load Files Thunk", () => {
    it("should create BEGIN_API_CALL and SORT_FILES_SUCCESS when sending files", () => {
      fetchMock.mock("*", {
        body: listFiles,
        headers: { "content-type": "application/json" },
      });

      const expectedActions = [
        { type: types.BEGIN_API_CALL },
        { type: types.SORT_FILES_SUCCESS, listFiles },
      ];

      const store = mockStore({ listFiles: [] });
      return store.dispatch(fileActions.sendListFiles()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
