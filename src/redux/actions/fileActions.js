import * as types from "./actionTypes";
import * as fileApi from "../../api/fileApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export function sortFilesSuccess(listFiles) {
  return { type: types.SORT_FILES_SUCCESS, listFiles };
}

export function sendListFiles(listFiles) {
  return function (dispatch) {
    dispatch(beginApiCall());
    return fileApi
      .sendListFiles(listFiles)
      .then((listeFilesResponse) => {
        dispatch(sortFilesSuccess(listeFilesResponse));
      })
      .catch((error) => {
        dispatch(apiCallError(error));
        throw error;
      });
  };
}
