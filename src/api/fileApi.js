import { handleResponse, handleError } from "./apiUtils";
const baseUrl = process.env.API_URL + "/files/";

export function sendListFiles(listFiles) {
  return fetch(baseUrl, {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify({ listFiles }),
  })
    .then(handleResponse)
    .catch(handleError);
}
