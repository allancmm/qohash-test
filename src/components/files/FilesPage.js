import React from "react";
import { connect } from "react-redux";
import { sendListFiles } from "../../redux/actions/fileActions";
import PropTypes from "prop-types";
import FolderList from "./FolderList";
import { toast } from "react-toastify";
import Spinner from "../common/Spinner";
import "./index.css";

const FilesPage = ({ sendListFiles, loading, listFiles }) => {
  const handleFolderFiles = async (folderFiles) => {
    if (folderFiles.length === 0) {
      toast.error("Folder is empty ", {
        autoClose: true,
      });
      return;
    }

    const liste = Array.from(folderFiles).map(
      ({ name, size, lastModified, webkitRelativePath: relativePath }) => {
        return { name, size, lastModified, relativePath };
      }
    );
    try {
      await sendListFiles(liste);
    } catch (error) {
      toast.error("Sending files failed. " + error.message, {
        autoClose: false,
      });
    }
  };

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-12 text-center">
            <h2>Qohash - Test</h2>
          </div>
        </div>

        <div className="row">
          <div className="col-12">
            <input
              className="btn btn-secundary directory-files pl-0"
              directory=""
              webkitdirectory=""
              type="file"
              onChange={(e) => handleFolderFiles(e.target.files)}
            />
          </div>
        </div>
        <div className="row mt-3">
          <div className="col-12">
            {loading ? (
              <Spinner />
            ) : (
              listFiles.map((dir) => (
                <FolderList key={dir.nameDirectory} dir={dir} />
              ))
            )}
          </div>
        </div>
      </div>
    </>
  );
};

FilesPage.propTypes = {
  sendListFiles: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  listFiles: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
  return {
    loading: state.apiCallsInProgress > 0,
    listFiles: state.files,
  };
}

const mapDispatchToProps = {
  sendListFiles,
};

export default connect(mapStateToProps, mapDispatchToProps)(FilesPage);
