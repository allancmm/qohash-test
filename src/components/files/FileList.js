import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { formatDate } from "../../utils/date";
import { formatBytes } from "../../utils/dataUnit";

const FileList = ({ files }) => {
  const [listeFilesSorted, setListeFilesSorted] = useState([]);

  useEffect(() => {
    setListeFilesSorted(files.sort((a, b) => b.size - a.size));
  }, [files]);

  return (
    <>
      {files.length > 0 ? (
        <>
          <div className="row">
            <div className="col-2 col-lg-1"></div>
            <div className="col-5 col-lg-5">
              <label className="font-weight-bold">File</label>
            </div>
            <div className="col-2 col-lg-4">
              <label className="font-weight-bold">Last Modified</label>
            </div>
            <div className="col-3 col-lg-2">
              <label className="font-weight-bold">Size</label>
            </div>
          </div>
          {listeFilesSorted.map((f) => (
            <div className="row" key={f.nameFile}>
              <div className="col-2 col-lg-1"></div>
              <div className="col-5 col-lg-5">
                <label>{f.nameFile}</label>
              </div>
              <div className="col-2 col-lg-4">
                <label>{formatDate(f.lastModified)}</label>
              </div>
              <div className="col-3 col-lg-2">
                <label>{formatBytes(f.size)}</label>
              </div>
            </div>
          ))}
        </>
      ) : null}
    </>
  );
};

FileList.propTypes = {
  files: PropTypes.array.isRequired,
};

export default FileList;
