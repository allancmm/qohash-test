import React, { useState } from "react";
import PropTypes from "prop-types";
import FileList from "./FileList";
import { formatBytes } from "../../utils/dataUnit";

const FolderList = ({ dir }) => {
  const [isShowSubDirectories, setShwoSubDirectories] = useState(false);

  const handleBtnShowSubDirectories = () => {
    setShwoSubDirectories(!isShowSubDirectories);
  };

  return (
    <>
      <div className="row">
        <div className="col-2 col-lg-1"></div>
        <div className="col-5 col-lg-5">
          <label className="font-weight-bold">Directory</label>
        </div>
        <div className="col-2 col-lg-4">
          <label className="font-weight-bold">Files</label>
        </div>
        <div className="col-3 col-lg-2">
          <label className="font-weight-bold">Total Size</label>
        </div>
      </div>
      <React.Fragment key={dir.nameDirectory}>
        <div className="row mb-2">
          <div className="col-2 col-lg-1">
            <input
              type="button"
              onClick={handleBtnShowSubDirectories}
              className="btn btn-outline-primary btn-sm"
              value={isShowSubDirectories ? "Close" : "Open"}
            />
          </div>
          <div className="col-5 col-lg-5">
            <label>{dir.nameDirectory}</label>
          </div>
          <div className="col-2 col-lg-4">
            <label>{dir.numberFiles}</label>
          </div>
          <div className="col-3 col-lg-2">
            <label>{formatBytes(dir.totalSize)}</label>
          </div>
        </div>

        {isShowSubDirectories && (
          <>
            <hr />
            <FileList files={dir.files} />
            {dir.subDirectories.length > 0 &&
              dir.subDirectories.map((sub) => (
                <FolderList key={sub.nameDirectory} dir={sub} />
              ))}
            <hr />
          </>
        )}
      </React.Fragment>
    </>
  );
};

FolderList.propTypes = {
  dir: PropTypes.object,
};

export default FolderList;
