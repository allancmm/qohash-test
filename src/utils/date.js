import moment from "moment";

export const formatDate = (date, format = "YYYY-MM-DD HH:mm:ss") => {
  return moment(date).locale("en").format(format);
};
