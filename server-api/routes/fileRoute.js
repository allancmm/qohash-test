const express = require("express");
const filesController = require("../controllers/filesController");

function routes() {
  const fileRouter = express.Router();
  const controller = filesController();
  fileRouter.route("/files").post(controller.post);
  return fileRouter;
}

module.exports = routes;
