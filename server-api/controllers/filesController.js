const { Directory, File } = require("../models/");

const filesController = () => {
  function post(req, res) {
    const { listFiles } = req.body;

    if (!listFiles || listFiles.length === 0) {
      res.status(400);
      return res.send("Files are required");
    }

    const listeGlobal = [];

    try {
      listFiles.forEach(({ size, lastModified, relativePath }) => {
        const arrayTokens = relativePath.split("/");
        createLevels(
          listeGlobal,
          size,
          lastModified,
          arrayTokens,
          arrayTokens.length - 1,
          0
        );
      });

      res.status(200);
      return res.json(listeGlobal);
    } catch (error) {
      res.status(500).send(error);
    }
  }
  return { post };
};

const createLevels = (
  listeP,
  size,
  lastModified,
  arrayTokensFile,
  maxLevels,
  currentLevel
) => {
  const directory = listeP.find(
    ({ nameDirectory }) => nameDirectory === arrayTokensFile[currentLevel]
  );

  if (directory) {
    directory.totalSize += size;
    directory.numberFiles++;
    if (currentLevel + 1 === maxLevels) {
      directory.files.push(
        new File(arrayTokensFile[currentLevel + 1], size, lastModified)
      );
      return;
    } else {
      createLevels(
        directory.subDirectories,
        size,
        lastModified,
        arrayTokensFile,
        maxLevels,
        currentLevel + 1
      );
    }
  } else {
    if (currentLevel + 1 === maxLevels) {
      listeP.push(
        new Directory(
          arrayTokensFile[currentLevel],
          size,
          [new File(arrayTokensFile[currentLevel + 1], size, lastModified)],
          1
        )
      );
      return;
    } else {
      const dir = new Directory(arrayTokensFile[currentLevel], size, [], 1);
      listeP.push(dir);
      createLevels(
        dir.subDirectories,
        size,
        lastModified,
        arrayTokensFile,
        maxLevels,
        currentLevel + 1
      );
    }
  }
};

module.exports = filesController;
