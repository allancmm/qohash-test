const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const compression = require("compression");

const app = express();

const port = process.env.PORT || 3001;
const fileRouter = require("./routes/fileRoute")();

app.use(compression());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: "200mb" }));

app.use(
  cors({
    origin: (origin, callback) => {
      const domains = ["localhost"];
      const isAuthorized = !!domains.find(function (domain) {
        return new RegExp(domain).test(origin);
      });

      console.log("isAuthorized", isAuthorized, origin);
      callback(isAuthorized ? null : "Bad request", isAuthorized);
    },
    credentials: true,
  })
);

app.use("/api", fileRouter);

app.get("/", (req, res) => {
  res.send("Welcome to Quohash Test API");
});

// app.server is used to stop the server after integrating tests
app.server = app.listen(port, () => console.log(`Running at port ${port}`));

module.exports = app;
