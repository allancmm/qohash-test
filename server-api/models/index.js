const Directory = require("../models/directoryModel");
const File = require("../models/fileModel");

module.exports = { Directory, File };
