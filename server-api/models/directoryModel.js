class Directory {
  constructor(nameDirectory, totalSize, files, numberFiles, subDirectories) {
    this.nameDirectory = nameDirectory || "";
    this.totalSize = totalSize || 0;
    this.files = files || [];
    this.numberFiles = numberFiles || 0;
    this.subDirectories = subDirectories || [];
  }
}

module.exports = Directory;
