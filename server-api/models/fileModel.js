class File {
  constructor(nameFile, size, lastModified) {
    this.nameFile = nameFile || "";
    this.size = size || 0;
    this.lastModified = lastModified || 0;
  }
}

module.exports = File;
