require("should");
const sinon = require("sinon");
const fileController = require("../controllers/filesController");

describe("File Controller Tests: ", () => {
  describe("Post", () => {
    it("should not allow an undefined list of files on post", () => {
      const req = {
        body: {},
      };

      const res = {
        status: sinon.spy(),
        send: sinon.spy(),
        json: sinon.spy(),
      };

      const controller = fileController();
      controller.post(req, res);
      res.status
        .calledWith(400)
        .should.equal(true, `Bad Status ${res.status.args[0][0]}`);

      res.send.calledWith("Files are required").should.equal(true);
    });
  });

  describe("Post", () => {
    it("should not allow an empty list of files on post", () => {
      const req = {
        body: {
          listFiles: [],
        },
      };

      const res = {
        status: sinon.spy(),
        send: sinon.spy(),
        json: sinon.spy(),
      };

      const controller = fileController();
      controller.post(req, res);
      res.status
        .calledWith(400)
        .should.equal(true, `Bad Status ${res.status.args[0][0]}`);

      res.send.calledWith("Files are required").should.equal(true);
    });
  });

  describe("Post", () => {
    it("should return the list of files on post", () => {
      const listeFiles = [
        {
          name: "test2.0.txt",
          size: 832,
          lastModified: 1596137302086,
          relativePath: "mainDirectory/directory2.0/test2.0.txt",
        },
      ];
      const req = {
        body: {
          listFiles: listeFiles,
        },
      };

      const res = {
        status: sinon.spy(),
        send: sinon.spy(),
        json: sinon.spy(),
      };

      const controller = fileController();
      controller.post(req, res);
      res.status.calledWith(200).should.equal(true);
    });
  });
});
